## Getting Started

- Language  : python 3.8
- FrameWork : falcon

- Make sure you have python3 installed
- Install pip. [Documentation](https://pip.pypa.io/en/stable/installation/)
- Create a virtual environment. [Documentation](https://docs.python.org/3/library/venv.html)
- Activate the virtual environment
- Install the dependencies using 

```
pip install -r requirements.txt
```
To run
```
python run.py
```
```
The server will be running on http://localhost:5000
Please use the postman collection doc shared to verify/test the APIs




```
[Postman Collection Doc](https://documenter.getpostman.com/view/9254806/UVBzopma)

## APIs Doc
- I have created seperate APIs for each subtask
- APIs are written in json request/response

	* AddAuthor

		POST API to add new author
		sample_request
		--------------
		{
		    "author_id" : "Author_001", 
		    "name": "JK",
		    "mobile" : "99999999999",
		    "date_of_birth" : "12-10-1800",
		    "death_date" : "12-10-1900"
		}
	
	* ListAuthors

		GET API for listing all authors data

	* AddBook

		POST API to add new book
		sample_request
		--------------
		{
		    "book_id" : "Book_001",
		    "title" : "Harry Potter",
		    "author_id" : "Author_001",
		    "publisher" : "DC Books",
		    "publish_date" : "12-10-1900",
		    "category_id" : "CAT001",
		    "price" : 1000,
		    "sold_count" : 1000
		}

	* GetMostBookSoldByAuthor

		POST API to get most sold books by a purticluar author
		sample_request
		--------------
		{
		    "author_id" : "Author_001"
		}



	* GetMostSoldBookByCategory

		POST API to get most sold books by a purticluar category
		sample_request
		--------------
		{
		    "category_id" : "CAT001"
		}


	* SearchBooks

		POST API to search books by book title or author name
		sample_request
		--------------
		{
		    "text" : "HA"
		}

	* ListBooksByAuthor

		POST API to list all books and can filter by author_id, 
			author_id is not mandatory and 
				if author_id is null will list all books 
		
		sample_request
		--------------

		{
		    "author_id" : "001"
		}

	* AddCategory

		POST API to add new category
		sample_request
		--------------

		{
		    "category_id" : "CAT001", 
		    "name": "Novel"
		}
	
	* ListCategory

		GET API for listing all categories data



## References
[Falcon](https://falcon.readthedocs.io/)
[SQLAlchemy Query](https://docs.sqlalchemy.org/en/14/orm/query.html)



