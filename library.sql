PGDMP                     
    y            library #   12.8 (Ubuntu 12.8-0ubuntu0.20.04.1) #   12.8 (Ubuntu 12.8-0ubuntu0.20.04.1)     �           0    0    ENCODING    ENCODING        SET client_encoding = 'UTF8';
                      false            �           0    0 
   STDSTRINGS 
   STDSTRINGS     (   SET standard_conforming_strings = 'on';
                      false            �           0    0 
   SEARCHPATH 
   SEARCHPATH     8   SELECT pg_catalog.set_config('search_path', '', false);
                      false            �           1262    466345    library    DATABASE     m   CREATE DATABASE library WITH TEMPLATE = template0 ENCODING = 'UTF8' LC_COLLATE = 'en_IN' LC_CTYPE = 'en_IN';
    DROP DATABASE library;
                postgres    false            �            1259    466372    author    TABLE       CREATE TABLE public.author (
    id integer NOT NULL,
    author_id character varying,
    name text,
    mobile text,
    date_of_birth character varying,
    death_date character varying,
    created timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);
    DROP TABLE public.author;
       public         heap    postgres    false            �            1259    466370    author_id_seq    SEQUENCE     �   CREATE SEQUENCE public.author_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 $   DROP SEQUENCE public.author_id_seq;
       public          postgres    false    207            �           0    0    author_id_seq    SEQUENCE OWNED BY     ?   ALTER SEQUENCE public.author_id_seq OWNED BY public.author.id;
          public          postgres    false    206            �            1259    466360    book    TABLE     ?  CREATE TABLE public.book (
    id integer NOT NULL,
    book_id character varying,
    title text,
    author_id character varying,
    publisher text,
    publish_date text,
    category_id character varying,
    price real,
    sold_count integer,
    created timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);
    DROP TABLE public.book;
       public         heap    postgres    false            �            1259    466358    book_id_seq    SEQUENCE     �   CREATE SEQUENCE public.book_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 "   DROP SEQUENCE public.book_id_seq;
       public          postgres    false    205            �           0    0    book_id_seq    SEQUENCE OWNED BY     ;   ALTER SEQUENCE public.book_id_seq OWNED BY public.book.id;
          public          postgres    false    204            �            1259    466348    category    TABLE     �   CREATE TABLE public.category (
    id integer NOT NULL,
    category_id character varying,
    name text,
    created timestamp without time zone DEFAULT CURRENT_TIMESTAMP
);
    DROP TABLE public.category;
       public         heap    postgres    false            �            1259    466346    category_id_seq    SEQUENCE     �   CREATE SEQUENCE public.category_id_seq
    AS integer
    START WITH 1
    INCREMENT BY 1
    NO MINVALUE
    NO MAXVALUE
    CACHE 1;
 &   DROP SEQUENCE public.category_id_seq;
       public          postgres    false    203            �           0    0    category_id_seq    SEQUENCE OWNED BY     C   ALTER SEQUENCE public.category_id_seq OWNED BY public.category.id;
          public          postgres    false    202            !           2604    466375 	   author id    DEFAULT     f   ALTER TABLE ONLY public.author ALTER COLUMN id SET DEFAULT nextval('public.author_id_seq'::regclass);
 8   ALTER TABLE public.author ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    207    206    207                       2604    466363    book id    DEFAULT     b   ALTER TABLE ONLY public.book ALTER COLUMN id SET DEFAULT nextval('public.book_id_seq'::regclass);
 6   ALTER TABLE public.book ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    204    205    205                       2604    466351    category id    DEFAULT     j   ALTER TABLE ONLY public.category ALTER COLUMN id SET DEFAULT nextval('public.category_id_seq'::regclass);
 :   ALTER TABLE public.category ALTER COLUMN id DROP DEFAULT;
       public          postgres    false    203    202    203            �          0    466372    author 
   TABLE DATA           a   COPY public.author (id, author_id, name, mobile, date_of_birth, death_date, created) FROM stdin;
    public          postgres    false    207   �       �          0    466360    book 
   TABLE DATA              COPY public.book (id, book_id, title, author_id, publisher, publish_date, category_id, price, sold_count, created) FROM stdin;
    public          postgres    false    205          �          0    466348    category 
   TABLE DATA           B   COPY public.category (id, category_id, name, created) FROM stdin;
    public          postgres    false    203   |       �           0    0    author_id_seq    SEQUENCE SET     ;   SELECT pg_catalog.setval('public.author_id_seq', 1, true);
          public          postgres    false    206            �           0    0    book_id_seq    SEQUENCE SET     9   SELECT pg_catalog.setval('public.book_id_seq', 1, true);
          public          postgres    false    204            �           0    0    category_id_seq    SEQUENCE SET     =   SELECT pg_catalog.setval('public.category_id_seq', 1, true);
          public          postgres    false    202            (           2606    466381    author author_pkey 
   CONSTRAINT     P   ALTER TABLE ONLY public.author
    ADD CONSTRAINT author_pkey PRIMARY KEY (id);
 <   ALTER TABLE ONLY public.author DROP CONSTRAINT author_pkey;
       public            postgres    false    207            &           2606    466369    book book_pkey 
   CONSTRAINT     L   ALTER TABLE ONLY public.book
    ADD CONSTRAINT book_pkey PRIMARY KEY (id);
 8   ALTER TABLE ONLY public.book DROP CONSTRAINT book_pkey;
       public            postgres    false    205            $           2606    466357    category category_pkey 
   CONSTRAINT     T   ALTER TABLE ONLY public.category
    ADD CONSTRAINT category_pkey PRIMARY KEY (id);
 @   ALTER TABLE ONLY public.category DROP CONSTRAINT category_pkey;
       public            postgres    false    203            �   I   x�3�t,-��/�700����D NC#]C]C��4202�54�50V00�25�20�330�4����� �[5      �   d   x�3�t��ώ700��H,*�T�/)I-�t,-��/��8+��s��Zp:;����l0ad`d�kh�k`�``he`beb�g`iidn����� ��@      �   5   x�3�tv100���/K��4202�54�50V00�2"K=c3s#�=... ��	�     