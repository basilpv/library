import falcon, json
from project.model.models import *


# add author
class AddAuthor:
    # @validate_token
    def on_post(self, req, res, user_id=True):
        try:
            if user_id:
                try:
                    json_data =json.loads(req.bounded_stream.read().decode('utf8'))
                    
                    author_id = json_data['author_id']
                    name = json_data['name']
                    mobile = json_data['mobile']
                    date_of_birth = json_data['date_of_birth']
                    try:
                        death_date = json_data['death_date']
                    except:
                        death_date = None

                except Exception as e:
                    status = 'error'
                    message = "Required fields missing. " + str(e)
                    res.status = falcon.HTTP_400
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 400
                        })
                    return

                try:
                    existing_author_id = session.query(Author).filter(Author.author_id==author_id).first()
                    if not existing_author_id:
                        
                        existing_mobile = session.query(Author).filter(Author.mobile==mobile).first()
                        if not existing_mobile:
                            
                            new_author = Author(
                                author_id=author_id,
                                name=name,
                                mobile=mobile,
                                date_of_birth=date_of_birth,
                                death_date=death_date,

                                )

                            session.add(new_author)

                            session.commit()

                            status = 'ok'
                            message = 'Success'
                            res.status = falcon.HTTP_200
                            res.text = json.dumps({ 
                                'status' : status, 
                                'message' : message,
                                'status_code' : 200
                                })
                            return
                            
                        else:
                            status = 'error'
                            message = "Mobile already exists."
                            res.status = falcon.HTTP_400
                            res.text = json.dumps({ 
                                'status' : status, 
                                'message' : message,
                                'status_code' : 400
                                })
                            return
                    else:
                        status = 'error'
                        message = 'Author ID already exists.'
                        res.status = falcon.HTTP_400
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'status_code' : 400
                            })
                        return
                    
                except Exception as e:
                    session.rollback()
                    status = 'error'
                    message = "Something went wrong : " +  str(e)
                    res.status = falcon.HTTP_500
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 500
                        })
                    return
            else:
                status = 'error'
                message = 'Unauthorized request'
                res.status = falcon.HTTP_401
                res.text = json.dumps({ 
                    'status' : status, 
                    'message' : message,
                    'status_code' : 401
                })
                return

        except Exception as e:
            session.rollback()
            status = 'error'
            message = "Something went wrong : " +  str(e)
            res.status = falcon.HTTP_500
            res.text = json.dumps({
                'status': status, 
                'message': message,
                'status_code' : 500
                })
            return


# list categories
class ListAuthors:
    # @validate_token
    def on_get(self, req, res, user_id=True):
        try:
            if user_id:
                try:
                    data = []
                    db_authors = session.query(Author).order_by(Author.name).all()
                    if db_authors:

                        for author in db_authors:
                            tempdata = {
                                'id' : author.id,
                                'author_id' : author.author_id,
                                'name' : author.name,
                                'mobile' : author.mobile,
                                'date_of_birth' : author.date_of_birth,
                                'death_date' : author.death_date,
                            }
                            data.append(tempdata)

                        status = 'ok'
                        message = 'Success'
                        res.status = falcon.HTTP_200
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'data' : data,
                            'status_code' : 200
                            })
                        return
                    else:
                        status = 'error'
                        message = 'Category ID already exists.'
                        res.status = falcon.HTTP_400
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'status_code' : 400
                            })
                        return
                    
                except Exception as e:
                    session.rollback()
                    status = 'error'
                    message = "Something went wrong : " +  str(e)
                    res.status = falcon.HTTP_500
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 500
                        })
                    return
            else:
                status = 'error'
                message = 'Unauthorized request'
                res.status = falcon.HTTP_401
                res.text = json.dumps({ 
                    'status' : status, 
                    'message' : message,
                    'status_code' : 401
                })
                return

        except Exception as e:
            session.rollback()
            status = 'error'
            message = "Something went wrong : " +  str(e)
            res.status = falcon.HTTP_500
            res.text = json.dumps({
                'status': status, 
                'message': message,
                'status_code' : 500
                })
            return






