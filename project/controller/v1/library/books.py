import falcon, json
from project.model.models import *
from project.controller.v1.library.dependencies import *
from sqlalchemy import or_


# add book to catelog
class AddBook:
    # @validate_token
    def on_post(self, req, res, user_id=True):
        try:
            if user_id:
                try:
                    json_data =json.loads(req.bounded_stream.read().decode('utf8'))
                    
                    book_id = json_data['book_id']
                    title = json_data['title']
                    author_id = json_data['author_id']
                    publisher = json_data['publisher']
                    publish_date = json_data['publish_date']
                    category_id = json_data['category_id']
                    price = float(json_data['price'])
                    sold_count = int(json_data['sold_count'])

                except Exception as e:
                    status = 'error'
                    message = "Required fields missing. " + str(e)
                    res.status = falcon.HTTP_400
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 400
                        })
                    return

                try:
                    existing_book_id = session.query(Book).filter(Book.book_id==book_id).first()
                    if not existing_book_id:
                        
                        existing_book_title = session.query(Book).filter(Book.title==title, Book.author_id==author_id).first()
                        if not existing_book_title:
                            
                            new_book = Book(
                                book_id=book_id,
                                title=title,
                                author_id=author_id,
                                publisher=publisher,
                                publish_date=publish_date,
                                category_id=category_id,
                                price=price,
                                sold_count=sold_count,
                                )
                            session.add(new_book)
                            session.commit()

                            status = 'ok'
                            message = 'Success'
                            res.status = falcon.HTTP_200
                            res.text = json.dumps({ 
                                'status' : status, 
                                'message' : message,
                                'status_code' : 200
                                })
                            return
                            
                        else:
                            status = 'error'
                            message = "Book title with same author already exists."
                            res.status = falcon.HTTP_400
                            res.text = json.dumps({ 
                                'status' : status, 
                                'message' : message,
                                'status_code' : 400
                                })
                            return
                    else:
                        status = 'error'
                        message = 'Book ID already exists.'
                        res.status = falcon.HTTP_400
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'status_code' : 400
                            })
                        return
                    
                except Exception as e:
                    session.rollback()
                    status = 'error'
                    message = "Something went wrong : " +  str(e)
                    res.status = falcon.HTTP_500
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 500
                        })
                    return
            else:
                status = 'error'
                message = 'Unauthorized request'
                res.status = falcon.HTTP_401
                res.text = json.dumps({ 
                    'status' : status, 
                    'message' : message,
                    'status_code' : 401
                })
                return

        except Exception as e:
            session.rollback()
            status = 'error'
            message = "Something went wrong : " +  str(e)
            res.status = falcon.HTTP_500
            res.text = json.dumps({
                'status': status, 
                'message': message,
                'status_code' : 500
                })
            return


# get most sold book by authors
class GetMostBookSoldByAuthor:
    # @validate_token
    def on_post(self, req, res, user_id=True):
        try:
            if user_id:
                try:
                    json_data =json.loads(req.bounded_stream.read().decode('utf8'))

                    author_id = json_data['author_id']

                except Exception as e:
                    status = 'error'
                    message = "Required fields missing. " + str(e)
                    res.status = falcon.HTTP_400
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 400
                        })
                    return
                try:
                    data = {}
                    db_books = session.query(Book, Author, Category
                        ).filter(
                            Book.author_id==Author.author_id,
                            Book.category_id==Category.category_id,
                            Author.author_id==author_id,
                            ).order_by(Book.sold_count.desc()).limit(1)

                    if db_books:

                        data = get_book_data(db_books)[0]

                        status = 'ok'
                        message = 'Success'
                        res.status = falcon.HTTP_200
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'data' : data,
                            'status_code' : 200
                            })
                        return
                    else:
                        status = 'error'
                        message = 'Category ID already exists.'
                        res.status = falcon.HTTP_400
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'status_code' : 400
                            })
                        return
                    
                except Exception as e:
                    session.rollback()
                    status = 'error'
                    message = "Something went wrong : " +  str(e)
                    res.status = falcon.HTTP_500
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 500
                        })
                    return
            else:
                status = 'error'
                message = 'Unauthorized request'
                res.status = falcon.HTTP_401
                res.text = json.dumps({ 
                    'status' : status, 
                    'message' : message,
                    'status_code' : 401
                })
                return

        except Exception as e:
            session.rollback()
            status = 'error'
            message = "Something went wrong : " +  str(e)
            res.status = falcon.HTTP_500
            res.text = json.dumps({
                'status': status, 
                'message': message,
                'status_code' : 500
                })
            return


# get most sold book by category
class GetMostSoldBookByCategory:
    # @validate_token
    def on_post(self, req, res, user_id=True):
        try:
            if user_id:
                try:
                    json_data =json.loads(req.bounded_stream.read().decode('utf8'))

                    category_id = json_data['category_id']

                except Exception as e:
                    status = 'error'
                    message = "Required fields missing. " + str(e)
                    res.status = falcon.HTTP_400
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 400
                        })
                    return
                try:
                    data = []
                    db_books = session.query(Book, Author, Category
                        ).filter(
                            Book.author_id==Author.author_id,
                            Book.category_id==Category.category_id,
                            Category.category_id==category_id,
                            ).order_by(Book.sold_count.desc()).limit(1)

                    if db_books:

                        data = get_book_data(db_books)[0]

                        status = 'ok'
                        message = 'Success'
                        res.status = falcon.HTTP_200
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'data' : data,
                            'status_code' : 200
                            })
                        return
                    else:
                        status = 'error'
                        message = 'Category ID already exists.'
                        res.status = falcon.HTTP_400
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'status_code' : 400
                            })
                        return
                    
                except Exception as e:
                    session.rollback()
                    status = 'error'
                    message = "Something went wrong : " +  str(e)
                    res.status = falcon.HTTP_500
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 500
                        })
                    return
            else:
                status = 'error'
                message = 'Unauthorized request'
                res.status = falcon.HTTP_401
                res.text = json.dumps({ 
                    'status' : status, 
                    'message' : message,
                    'status_code' : 401
                })
                return

        except Exception as e:
            session.rollback()
            status = 'error'
            message = "Something went wrong : " +  str(e)
            res.status = falcon.HTTP_500
            res.text = json.dumps({
                'status': status, 
                'message': message,
                'status_code' : 500
                })
            return


# serch books by author/book
class SearchBooks:
    # @validate_token
    def on_post(self, req, res, user_id=True):
        try:
            if user_id:
                try:
                    json_data =json.loads(req.bounded_stream.read().decode('utf8'))

                    text = json_data['text']
                    text = '%'+text+'%'

                except Exception as e:
                    status = 'error'
                    message = "Required fields missing. " + str(e)
                    res.status = falcon.HTTP_400
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 400
                        })
                    return
                try:
                    data = []
                    db_books = session.query(Book, Author, Category
                        ).filter(
                            Book.author_id==Author.author_id,
                            Book.category_id==Category.category_id,
                            or_(
                                Book.title.ilike(text),
                                Author.name.ilike(text),
                            )
                            ).order_by(Book.title).all()

                    if db_books:

                        data = get_book_data(db_books)

                        status = 'ok'
                        message = 'Success'
                        res.status = falcon.HTTP_200
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'data' : data,
                            'status_code' : 200
                            })
                        return
                    else:
                        status = 'error'
                        message = 'Books not found.'
                        res.status = falcon.HTTP_400
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'status_code' : 400
                            })
                        return
                    
                except Exception as e:
                    session.rollback()
                    status = 'error'
                    message = "Something went wrong : " +  str(e)
                    res.status = falcon.HTTP_500
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 500
                        })
                    return
            else:
                status = 'error'
                message = 'Unauthorized request'
                res.status = falcon.HTTP_401
                res.text = json.dumps({ 
                    'status' : status, 
                    'message' : message,
                    'status_code' : 401
                })
                return

        except Exception as e:
            session.rollback()
            status = 'error'
            message = "Something went wrong : " +  str(e)
            res.status = falcon.HTTP_500
            res.text = json.dumps({
                'status': status, 
                'message': message,
                'status_code' : 500
                })
            return


# list books by author
class ListBooksByAuthor:
    # @validate_token
    def on_post(self, req, res, user_id=True):
        try:
            if user_id:
                try:
                    json_data =json.loads(req.bounded_stream.read().decode('utf8'))

                    try:
                        author_id = json_data['author_id']
                    except:
                        author_id = None

                except Exception as e:
                    status = 'error'
                    message = "Required fields missing. " + str(e)
                    res.status = falcon.HTTP_400
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 400
                        })
                    return
                try:
                    data = []
                    if author_id:
                        db_books = session.query(Book, Author, Category
                            ).filter(
                                Book.author_id==Author.author_id,
                                Book.category_id==Category.category_id,
                                Author.author_id==author_id,
                                ).order_by(Book.title).all()
                    else:
                        db_books = session.query(Book, Author, Category
                            ).filter(
                                Book.author_id==Author.author_id,
                                Book.category_id==Category.category_id,
                                Author.author_id==author_id,
                                ).order_by(Book.title).all()


                    if db_books:

                        data = get_book_data(db_books)

                        status = 'ok'
                        message = 'Success'
                        res.status = falcon.HTTP_200
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'data' : data,
                            'status_code' : 200
                            })
                        return
                    else:
                        status = 'error'
                        message = 'Books not found.'
                        res.status = falcon.HTTP_400
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'status_code' : 400
                            })
                        return
                    
                except Exception as e:
                    session.rollback()
                    status = 'error'
                    message = "Something went wrong : " +  str(e)
                    res.status = falcon.HTTP_500
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 500
                        })
                    return
            else:
                status = 'error'
                message = 'Unauthorized request'
                res.status = falcon.HTTP_401
                res.text = json.dumps({ 
                    'status' : status, 
                    'message' : message,
                    'status_code' : 401
                })
                return

        except Exception as e:
            session.rollback()
            status = 'error'
            message = "Something went wrong : " +  str(e)
            res.status = falcon.HTTP_500
            res.text = json.dumps({
                'status': status, 
                'message': message,
                'status_code' : 500
                })
            return