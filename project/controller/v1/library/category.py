import falcon, json
from project.model.models import *

# add category
class AddCategory:
    # @validate_token
    def on_post(self, req, res, user_id=True):
        try:
            if user_id:
                try:
                    json_data =json.loads(req.bounded_stream.read().decode('utf8'))
                    
                    category_id = json_data['category_id']
                    name = json_data['name']

                except Exception as e:
                    status = 'error'
                    message = "Required fields missing. " + str(e)
                    res.status = falcon.HTTP_400
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 400
                        })
                    return

                try:
                    existing_category_id = session.query(Category).filter(Category.category_id==category_id).first()
                    if not existing_category_id:

                        new_category = Category(
                            category_id=category_id,
                            name=name
                            )
                        session.add(new_category)
                        session.commit()

                        status = 'ok'
                        message = 'Success'
                        res.status = falcon.HTTP_200
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'status_code' : 200
                            })
                        return
                    else:
                        status = 'error'
                        message = 'Category ID already exists.'
                        res.status = falcon.HTTP_400
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'status_code' : 400
                            })
                        return
                    
                except Exception as e:
                    session.rollback()
                    status = 'error'
                    message = "Something went wrong : " +  str(e)
                    logger.error(req.url+"|"+str(user_id)+"|"+"["+self.__class__.__name__+"]| " +message)
                    res.status = falcon.HTTP_500
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 500
                        })
                    return
            else:
                status = 'error'
                message = 'Unauthorized request'
                res.status = falcon.HTTP_401
                res.text = json.dumps({ 
                    'status' : status, 
                    'message' : message,
                    'status_code' : 401
                })
                return

        except Exception as e:
            session.rollback()
            status = 'error'
            message = "Something went wrong : " +  str(e)
            res.status = falcon.HTTP_500
            res.text = json.dumps({
                'status': status, 
                'message': message,
                'status_code' : 500
                })
            return





# list categories
class ListCategory:
    # @validate_token
    def on_get(self, req, res, user_id=True):
        try:
            if user_id:
                try:
                    data = []
                    db_categories = session.query(Category).order_by(Category.name).all()
                    if db_categories:

                        for category in db_categories:
                            tempdata = {
                                'id' : category.id,
                                'category_id' : category.category_id,
                                'name' : category.name,
                            }
                            data.append(tempdata)

                        status = 'ok'
                        message = 'Success'
                        res.status = falcon.HTTP_200
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'data' : data,
                            'status_code' : 200
                            })
                        return
                    else:
                        status = 'error'
                        message = 'Category ID already exists.'
                        res.status = falcon.HTTP_400
                        res.text = json.dumps({ 
                            'status' : status, 
                            'message' : message,
                            'status_code' : 400
                            })
                        return
                    
                except Exception as e:
                    session.rollback()
                    status = 'error'
                    message = "Something went wrong : " +  str(e)
                    logger.error(req.url+"|"+str(user_id)+"|"+"["+self.__class__.__name__+"]| " +message)
                    res.status = falcon.HTTP_500
                    res.text = json.dumps({
                        'status': status, 
                        'message': message,
                        'status_code' : 500
                        })
                    return
            else:
                status = 'error'
                message = 'Unauthorized request'
                res.status = falcon.HTTP_401
                res.text = json.dumps({ 
                    'status' : status, 
                    'message' : message,
                    'status_code' : 401
                })
                return

        except Exception as e:
            session.rollback()
            status = 'error'
            message = "Something went wrong : " +  str(e)
            res.status = falcon.HTTP_500
            res.text = json.dumps({
                'status': status, 
                'message': message,
                'status_code' : 500
                })
            return