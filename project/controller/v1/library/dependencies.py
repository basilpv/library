import falcon, json
from project.model.models import *

#for list and process books data
def get_book_data(db_books):
    data = []
    for book, author, catgeory in db_books:
        tempdata = {
            'books_info' : {
                'book_id' : book.book_id,
                'title' : book.title,
                'author_id' : book.author_id,
                'publisher' : book.publisher,
                'publish_date' : book.publish_date,
                'category_id' : book.category_id,
                'price' : book.price,
                'sold_count' : book.sold_count,
            },
            'author_info' : {
                'author_id' : author.author_id,
                'name' : author.name,
                'mobile' : author.mobile,
                'date_of_birth' : author.date_of_birth,
                'death_date' : author.death_date,
            },

            'category_info' : {
                'category_id' : catgeory.category_id,
                'name' : catgeory.name,
            }
        }
        data.append(tempdata)

    return data