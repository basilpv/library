from sqlalchemy.ext.automap import automap_base
from sqlalchemy import create_engine
from sqlalchemy.orm import Session, relationship
from project.config import SQLALCHEMY_DATABASE_URI


Base = automap_base()
engine = create_engine(SQLALCHEMY_DATABASE_URI)
Base.prepare(engine,reflect=True)
session = Session(engine)

# tables created
Author = Base.classes.author
Book = Base.classes.book
Category = Base.classes.category

















