import falcon
from falcon_cors import CORS
from project.controller import *

cors = CORS(allow_all_origins=True, allow_all_headers=True, allow_methods_list=['GET', 'POST', 'OPTIONS'])
app = falcon.App(middleware=[cors.middleware])

# author
app.add_route("/api/v1/author/add", AddAuthor())
app.add_route("/api/v1/author/list", ListAuthors())

# book
app.add_route("/api/v1/book/add", AddBook())
app.add_route("/api/v1/book/list/by-author", GetMostBookSoldByAuthor())
app.add_route("/api/v1/book/list/by-category", GetMostSoldBookByCategory())
app.add_route("/api/v1/book/list/search", SearchBooks())
app.add_route("/api/v1/book/list/all", ListBooksByAuthor())

#category
app.add_route("/api/v1/category/add", AddCategory())
app.add_route("/api/v1/category/list", ListCategory())



















































































































































